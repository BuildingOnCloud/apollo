# Apollo

This is my first MVP DevOps Technologies POC
The purpose of this project is learn, show and apply first hand experience in cutting edge DevOps technologies, with this project I pretend get expertise in:

1) GitLab platform use.
2) Git branching strategies.
3) Create a full CI/CD pipelines.
4) Integrate external tools to devops process.
5) Use Container & Kubernetes platform.
6) Strugle with day to day DevOps challenges.
7) Use this project to test new knowledge.

To accomplish this goals, I need spend time with know more about pros and cons about tools, approach and solutions. All this project is learning for me, and I hope to help someone look for learn more about DevOps.

### The Beginning:
First at all, we need organize the differents stage of this project, I prefer split the works on several tasks:

1) [Project General Setup](./project_general_setup.md)
2) [Branching strategies](./branching_strategies.md)
3) Our App

This is in progress work...
